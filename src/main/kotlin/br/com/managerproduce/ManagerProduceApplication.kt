package br.com.managerproduce

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ManagerProduceApplication

fun main(args: Array<String>) {

	runApplication<ManagerProduceApplication>(*args)
}
