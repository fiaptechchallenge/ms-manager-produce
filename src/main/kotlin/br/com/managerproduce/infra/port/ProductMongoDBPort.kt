package br.com.managerproduce.infra.port

import br.com.managerproduce.infra.enums.ProductCategory
import br.com.managerproduce.infra.model.ProductDocument
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository

interface ProductMongoDBPort: MongoRepository<ProductDocument, ObjectId> {

    fun findByCode(code: String): ProductDocument?

    fun findByCategory(category: ProductCategory): MutableList<ProductDocument>?
}