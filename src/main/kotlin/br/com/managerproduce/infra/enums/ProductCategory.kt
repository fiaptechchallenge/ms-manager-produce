package br.com.managerproduce.infra.enums

enum class ProductCategory {
    SNACK,
    DRINK,
    ACCOMPANIMENT,
    COMBO
}