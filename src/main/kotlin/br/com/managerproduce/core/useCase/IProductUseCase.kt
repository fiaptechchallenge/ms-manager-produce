package br.com.managerproduce.core.useCase

import br.com.managerproduce.infra.enums.ProductCategory
import br.com.managerproduce.core.domain.Product

interface IProductUseCase {

    fun create(product: Product): Product

    fun listAll(): MutableList<Product>

    fun findByCode(code: String): Product

    fun findByCategory(category: ProductCategory): MutableList<Product>

    fun update(code: String, product: Product)

    fun remove(code: String)
}