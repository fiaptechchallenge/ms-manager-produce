package br.com.managerproduce.core.domain.exception

class NotFoundException(
        override val message: String?,
        override val cause: Throwable? = null
): Exception(message, cause) {

}