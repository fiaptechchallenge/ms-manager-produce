package br.com.managerproduce.core.domain.response

import br.com.managerproduce.infra.enums.ProductCategory
import java.time.LocalDateTime

data class ProductResponse(
        val name: String,
        val code: String,
        val price: Double,
        val quantity:  Number,
        val category: ProductCategory,
        val dateValidate: LocalDateTime
)
