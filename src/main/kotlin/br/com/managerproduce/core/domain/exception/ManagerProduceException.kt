package br.com.managerproduce.core.domain.exception

import org.springframework.http.HttpStatus

class ManagerProduceException(
        override val message: String?,
        val code: HttpStatus,
        override val cause: Throwable? = null
): Exception(message, cause)