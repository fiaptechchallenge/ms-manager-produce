package br.com.managerproduce.core.repositoryService

import br.com.managerproduce.infra.enums.ProductCategory
import br.com.managerproduce.core.domain.Product

interface ProductRepositoryService {

    fun create(product: Product): Product

    fun findByCategory(category: ProductCategory): MutableList<Product>

    fun findByAll(): MutableList<Product>

    fun findByCode(code: String): Product?

    fun delete(product: Product)

}